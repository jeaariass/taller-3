//funcion anonima, ALMACENADA A UNA VARIABLE !!!!
let a = function(mensaje) {
        console.log(mensaje);
        return true;
    }
    //funcion flecha
let b = (mensaje) => {
    console.log("su numero es " + mensaje);
    return true;
}

//callback, funcion que llama funciones
let f = (palabra) => {
    a(palabra);
}